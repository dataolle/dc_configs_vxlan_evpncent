#!/bin/bash

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR at last command. Aborting this launch script"
    echo "Please check the Production Ready Automation user guide at https://docs.cumulusnetworks.com for system dependencies and prerequisites and to try to start the simulation manually"
    echo "Ask for help on the Cumulus Community public Slack: https://slack.cumulusnetworks.com"
    exit 1
fi
}


if [ ! "$(ls -A cldemo2)" ]
then
    echo "Cumulus Reference topology submodule not present"
    echo "attempting to fix that for us"
    cd cldemo2
    check_state
    git submodule init
    check_state
    git submodule update
    check_state
    cd ..
    check_state
else
    echo "Submodule folder not empty. Assuming it is ok"
fi

cd cldemo2/simulation
check_state

echo "Starting OOB management devices"
if [ "$1" == "--no-netq" ]; then
  vagrant up oob-mgmt-server oob-mgmt-switch
  check_state
else
  vagrant up oob-mgmt-switch oob-mgmt-server netq-ts
  check_state
fi

echo "Starting the Network nodes"
vagrant up leaf01 leaf02 leaf03 leaf04 spine01 spine02 spine03 spine04 
check_state
vagrant up server01 server02 server03 server04 
check_state
vagrant up server05 server06 server07 server08 
check_state
vagrant up border01 border02 fw1 fw2
check_state

echo "Copy Topology Automation to oob-mgmt-server"
BRANCH=`git rev-parse --abbrev-ref HEAD`
vagrant ssh oob-mgmt-server -c "git clone -b $BRANCH https://gitlab.com/cumulus-consulting/goldenturtle/dc_configs_vxlan_evpncent.git"
check_state
echo "Finsihed automation copy into the simulation"
echo ""

# VX nodes in simulation take 3-5 mins after they finish loading from vagrant here until they can be deployed to with ansible
# ZTP has to run, then a second reboot occurs. In theory, we could also provision the simulation from this script
# but we would have to bake in a long-ish delay to allow for everything to finish rebooting
# something like this:
# vagrant ssh oob-mgmt-server -c "cd dc_configs_vxlan_evpncent/automation && ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff"

echo ""
echo "Displaying status of all devices under this Vagrant simulation"
echo "netq-ts may not be running if you used the --no-netq option"
echo ""
vagrant status

echo ""
echo "###########################"
echo "# Demo launch complete!   #"
echo "###########################"
echo ""
echo "Change to cldemo2/simluation directory to vagrant ssh into the simulation:"
echo ""
echo "cd cldemo2/simluation"
echo "vagrant ssh oob-mgmt-server"
echo ""
echo "Once inside the simulation, cd to dc_configs_vxlan_evpncent/automation"
echo "and use the command: ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff"
echo "to configure the simulation with this demo"
echo ""

